FROM php:7.4-cli-alpine

RUN apk add --no-cache --virtual .persistent-deps libffi-dev \
    && docker-php-ext-configure ffi --with-ffi \
    && docker-php-ext-install ffi \
    && apk add --no-cache php7-phpdbg
