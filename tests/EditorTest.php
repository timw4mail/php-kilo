<?php declare(strict_types=1);

namespace Aviat\Kilo\Tests\Traits;

use Aviat\Kilo\Editor;
use PHPUnit\Framework\TestCase;
use Spatie\Snapshots\MatchesSnapshots;
use function Aviat\Kilo\get_window_size;

class MockEditor extends Editor {
	public function __set(string $key, $value): void
	{
		if (property_exists($this, $key))
		{
			$this->$key = $value;
		}
	}
}

class EditorTest extends TestCase {
	use MatchesSnapshots;

	protected Editor $editor;

	public function setUp(): void
	{
		parent::setUp();

		// Mock the screen size, since that can vary widely
		$this->editor = MockEditor::new();
		$this->editor->__set('screenRows', 23);
		$this->editor->__set('screenCols', 80);
	}

	public function testSanity(): void
	{
		$this->assertEquals(0, $this->editor->numRows);
		$this->assertNull($this->editor->syntax);
	}

	public function test__debugInfo(): void
	{
		$state = json_encode($this->editor->__debugInfo(), JSON_THROW_ON_ERROR);
		$this->assertMatchesJsonSnapshot($state);
	}

	public function testOpenPHP(): void
	{
		$this->editor->open('test.php');
		$state = json_encode($this->editor, JSON_THROW_ON_ERROR);

		$this->assertMatchesJsonSnapshot($state);
	}

	public function testOpen(): void
	{
		$this->editor->open('src/ffi.h');
		$state = json_encode($this->editor, JSON_THROW_ON_ERROR);

		$this->assertMatchesJsonSnapshot($state);
	}
}