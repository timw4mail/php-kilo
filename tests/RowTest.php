<?php declare(strict_types=1);

namespace Aviat\Kilo\Tests\Traits;

use Aviat\Kilo\{Editor, Row};
use PHPUnit\Framework\TestCase;

class RowTest extends TestCase {
	protected Editor $editor;
	protected Row $row;

	public function setUp(): void
	{
		parent::setUp();

		$this->editor = Editor::new();
		$this->row = Row::new($this->editor, '', 0);
	}

	public function testSanity(): void
	{
		$this->assertEquals(0, $this->row->size);
		$this->assertEquals(0, $this->row->rsize);
		$this->assertNull($this->row->foo);
		$this->assertEmpty($this->row->chars);
		$this->assertEmpty($this->row->render);
	}

	public function testSetRunsUpdate(): void
	{
		$this->row->chars = 'abcde';
		$this->assertEquals('abcde', $this->row->render);
	}

	public function test__toString(): void
	{
		$this->row->chars = 'abcde';
		$this->assertEquals("abcde\n", (string)$this->row);
	}

	public function test__debugInfo(): void
	{
		$actual = $this->row->__debugInfo();
		$expected = [
			'size' => 0,
			'rsize' => 0,
			'chars' => '',
			'render' => '',
			'hl' => [],
			'hlOpenComment' => FALSE,
		];

		$this->assertEquals($expected, $actual);
	}

	public function testInsertChar(): void
	{
		$this->row->chars = 'abde';
		$this->row->insertChar(2, 'c');

		$this->assertEquals('abcde', $this->row->chars);
		$this->assertEquals('abcde', $this->row->render);
		$this->assertEquals(1, $this->editor->dirty);
	}

	public function testInsertCharBadOffset(): void
	{
		$this->row->chars = 'ab';
		$this->row->insertChar(5, 'c');

		$this->assertEquals('abc', $this->row->chars);
		$this->assertEquals('abc', $this->row->render);
		$this->assertEquals(1, $this->editor->dirty);
	}

	public function testDeleteChar(): void
	{
		$this->row->chars = 'abcdef';
		$this->row->deleteChar(5);

		$this->assertEquals('abcde', $this->row->chars);
		$this->assertEquals('abcde', $this->row->render);
		$this->assertEquals(1, $this->editor->dirty);
	}

	public function testDeleteCharBadOffset(): void
	{
		$this->row->chars = 'ab';
		$this->row->deleteChar(5);

		$this->assertEquals('ab', $this->row->chars);
		$this->assertEquals(0, $this->editor->dirty);
	}
}