<?php declare(strict_types=1);

namespace Aviat\Kilo\Tests\Traits;

use Aviat\Kilo\Traits\ConstList;
use PHPUnit\Framework\TestCase;

class ConstListTest extends TestCase {
	public function testGetConstList(): void
	{
		$testClass = new class {
			use ConstList;

			public const foo = 'foo';
			public const bar = 'bar';
		};

		$expected = [
			'foo' => 'foo',
			'bar' => 'bar',
		];

		$this->assertEquals($expected, $testClass::getConstList());
	}
}
