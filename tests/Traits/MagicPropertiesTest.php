<?php declare(strict_types=1);

namespace Aviat\Kilo\Tests\Traits;

use Aviat\Kilo\Traits\MagicProperties;
use PHPUnit\Framework\TestCase;

class MagicPropertiesTest extends TestCase {
	protected object $testClass;

	public function __construct($name = NULL, array $data = [], $dataName = '')
	{
		parent::__construct($name, $data, $dataName);

		$this->testClass = new class {
			use MagicProperties;

			protected string $foo = 'foo';
			protected int $bar = 0x7b;

			public function __get(string $name)
			{
				if ($this->__isset($name))
				{
					return $this->$name;
				}

				return NULL;
			}
		};
	}

	public function test__get(): void
	{
		$this->assertEquals('foo', $this->testClass->__get('foo'));
		$this->assertNull($this->testClass->__get('fooBar'));
	}

	/**
	 * @depends test__get
	 */
	public function test__isset(): void
	{
		$this->assertTrue($this->testClass->__isset('foo'));
		$this->assertFalse($this->testClass->__isset('fooBar'));
	}

	/**
	 * @depends test__get
	 */
	public function test__set(): void
	{
		$this->testClass->__set('foo', 'baz');
		$this->assertEquals('baz', $this->testClass->__get('foo'));

		$this->testClass->__set('baz', []);
		$this->assertFalse($this->testClass->__isset('baz'));
		$this->assertNull($this->testClass->baz);
	}
}