<?php declare(strict_types=1);

namespace Aviat\Kilo\Tests;

use Aviat\Kilo\Event;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class EventTest extends TestCase {
	public function testRequiresValidEvent(): void
	{
		$this->expectException(InvalidArgumentException::class);
		Event::on('badEventName', fn () => null);

		$this->expectException(InvalidArgumentException::class);
		Event::fire('badEventName', []);
	}

	public function testBindAndFire(): void
	{
		$fn = static function($value = false) {
			static::assertTrue($value);
		};
		Event::on(Event::INPUT_KEY, $fn);
		Event::fire(Event::INPUT_KEY, TRUE);
	}
}
