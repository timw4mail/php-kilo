<?php declare(strict_types=1);

namespace Aviat\Kilo\Tests\Traits;

use Aviat\Kilo\ANSI;
use Aviat\Kilo\Enum\Color;
use PHPUnit\Framework\TestCase;

class ANSITest extends TestCase {
	public function testColor():void
	{
		$this->assertEquals("\e[44m", ANSI::color(Color::BG_BLUE));
	}

	public function testRgbColor(): void
	{
		$this->assertEquals("\e[38;2;128;128;128m", ANSI::rgbColor(128, 128, 128));
	}

	public function testMoveCursor(): void
	{
		$this->assertEquals("\e[25;40H", ANSI::moveCursor(25, 40));
	}

	public function testScrollUp(): void
	{
		$this->assertEquals("\e[5S", ANSI::scrollUp(5));
	}

	public function testScrollDown(): void
	{
		$this->assertEquals("\e[5T", ANSI::scrollDown(5));
	}
}