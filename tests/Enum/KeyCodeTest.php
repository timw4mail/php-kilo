<?php declare(strict_types=1);

namespace Aviat\Kilo\Tests\Enum;

use function Aviat\Kilo\ctrl_key;

use Aviat\Kilo\Enum\KeyCode;
use PHPUnit\Framework\TestCase;

class KeyCodeTest extends TestCase {
	public function testSanityCheck(): void
	{
		for ($i = 1; $i < 27; $i++)
		{
			$char = chr(0x60 + $i);
			$ord = $i;
			$expected = chr($ord);

			$actual = KeyCode::CTRL($char);

			$this->assertEquals(ctrl_key($char), $ord, "chr(ctrl_key) !== CTRL");
			$this->assertEquals($expected, $actual, "CTRL+'{$char}' should return chr($ord)");
		}
	}

	public function testNullOnInvalidChar(): void
	{
		$this->assertNull(KeyCode::CTRL("\t"));
	}

	public function testSameOutputOnUpperOrLower(): void
	{
		$lower = KeyCode::CTRL('v');
		$upper = KeyCode::CTRL('V');

		$this->assertEquals($lower, $upper);
	}
}
