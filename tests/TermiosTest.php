<?php declare(strict_types=1);

namespace Aviat\Kilo\Tests\Traits;

use Aviat\Kilo\{Termios, TermiosException};
use PHPUnit\Framework\TestCase;

class TermiosTest extends TestCase {
	public function testEnableRawMode(): void
	{
		// There will be an exception, due to the way the test suite is run
		$this->expectException(TermiosException::class);
		$this->assertNotEquals(NULL, Termios::enableRawMode());
	}

	/**
	 * @depends testEnableRawMode
	 */
	public function testEnableRowModeTwice(): void
	{
		$this->assertNull(Termios::enableRawMode());
	}

	/**
	 * @depends testEnableRawMode
	 */
	public function testDisableRawMode(): void
	{
		// There will be an exception, due to the way the test suite is run
		$this->expectException(TermiosException::class);
		$this->assertFalse(Termios::disableRawMode());
	}
}