# PHP Kilo

[![Build Status](https://jenkins.timshomepage.net/buildStatus/icon?job=timw4mail%2Fphp-kilo%2Fmaster)](https://jenkins.timshomepage.net/job/timw4mail/job/php-kilo/job/master/)

A reimplementation of the [Kilo](https://viewsourcecode.org/snaptoken/kilo/index.html) tutorial in PHP. Requires PHP 7.4,
due to requiring the `FFI` extension.

## Implementation notes:

* The `editor` prefix has been removed from all the relevant functions, instead they are methods on the `Editor` class.
* Enums are faked with class constants
* Composer is used for autoloading
* Properties that must be manually updated in the C version (like counts/string length) are implemented with magic methods,
so they are essentially calculated on read.
* Generally, if a function exists in PHP, with the same name as the C function, the PHP version will be used.