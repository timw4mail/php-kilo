<?php declare(strict_types=1);

interface Ifoo {}

abstract class Foo implements Ifoo {
	/**
	 * @param int $a
	 * @param float $b
	 * @param array $c
	 * @param callable $d
	 * @return string
	 */
	abstract public function bar(int $a, float $b, array $c, callable $d): string;

	protected function doNothing(): void {}
}

/**
 * Docblock comment
 */
class FooBar extends Foo implements Ifoo {
	public function bar(int $a, float $b, array $c, callable $d, string $e = 'default'): string
	{
		$cstr = print_r($c, TRUE);
		$d();

		return "{$a}, ${b}, " . $cstr;
	}

	private function operations(int $a, int $b): int
	{
		$this->doNothing();

		$c = $a + $b;
		$a = $c - $b;

		$c = $a * $b;
		$b = (int) ($c / $a);

		return $c;
	}
}

/*
 * Multi-line comment
 */
$foobar = new FooBar();

$baz = ['a' => 'b'];

// C++ style comment
$x = 3;

# Perl-style comment
$y = [
	1,
	2,
	3
];

// Multi-line ternary statement
$q = ($x !== 2)
	? 'yes'
	: 'no';

/*
Heredoc
*/$z = $x + $y;
$sql = <<<SQL
	SELECT * FROM "foo" WHERE "bar"='baz' AND id={$x};
SQL;

/* Nowdoc */
$template = <<<'TEMPLATE'
	<foo>{x}</foo>
TEMPLATE;

?>
<html lang="en">
<body>
<h1><?= $_SERVER['HTTP_HOST'] ?></h1>
</body>
</html>
<?php exit(); ?>