<?php declare(strict_types=1);

namespace Aviat\Kilo;

// -----------------------------------------------------------------------------
// ! App Constants
// -----------------------------------------------------------------------------
const KILO_VERSION = '0.3.0';
const KILO_TAB_STOP = 4;
const KILO_QUIT_TIMES = 3;
