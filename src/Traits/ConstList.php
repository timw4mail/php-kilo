<?php declare(strict_types=1);

namespace Aviat\Kilo\Traits;

use ReflectionClass;

trait ConstList {
	public static function getConstList(): array
	{
		return (new ReflectionClass(static::class))->getConstants();
	}
}