<?php declare(strict_types=1);

namespace Aviat\Kilo\Traits;

trait MagicProperties {
	abstract public function __get(string $name);

	public function __set(string $name, $value)
	{
		if (property_exists($this, $name))
		{
			$this->$name = $value;
		}
	}

	public function __isset(string $name): bool
	{
		return isset($this->$name);
	}
}