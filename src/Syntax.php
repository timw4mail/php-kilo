<?php declare(strict_types=1);

namespace Aviat\Kilo;

class Syntax {
	public const HIGHLIGHT_NUMBERS = (1 << 0);
	public const HIGHLIGHT_STRINGS = (1 << 1);

	public string $filetype = '';
	public array $filematch = [];

	public string $singleLineCommentStart = '//';
	public string $multiLineCommentStart = '/*';
	public string $multiLineCommentEnd = '*/';

	public array $keywords1 = [];
	public array $keywords2 = [];

	// Tokens for PHP files
	public array $tokens = [];

	public int $flags = 0;

	public static function new(string $name, array $extList, array $keywords1, array $keywords2, string $slcs, string $mcs, string $mce, int $flags): self
	{
		$self = new self();

		$self->filetype = $name;
		$self->filematch = $extList;

		$self->keywords1 = $keywords1;
		$self->keywords2 = $keywords2;

		$self->singleLineCommentStart = $slcs;
		$self->multiLineCommentStart = $mcs;
		$self->multiLineCommentEnd = $mce;

		$self->flags = $flags;

		return $self;
	}

	private function __construct() {}
}