<?php declare(strict_types=1);

namespace Aviat\Kilo\Enum;

use Aviat\Kilo\Traits;

/**
 * Constants representing various control keys
 */
class KeyType {
	use Traits\ConstList;

	public const ARROW_DOWN = 'ARROW_DOWN';
	public const ARROW_LEFT = 'ARROW_LEFT';
	public const ARROW_RIGHT = 'ARROW_RIGHT';
	public const ARROW_UP = 'ARROW_UP';
	public const BACKSPACE = 'BACKSPACE';
	public const DEL_KEY = 'DELETE';
	public const END_KEY = 'END';
	public const ENTER = 'ENTER';
	public const ESCAPE = 'ESCAPE';
	public const HOME_KEY = 'HOME';
	public const PAGE_DOWN = 'PAGE_DOWN';
	public const PAGE_UP = 'PAGE_UP';
}