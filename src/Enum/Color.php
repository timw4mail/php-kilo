<?php declare(strict_types=1);

namespace Aviat\Kilo\Enum;

use Aviat\Kilo\Traits;

/**
 * ANSI Color escape sequences
 */
class Color {
	use Traits\ConstList;

	// Foreground colors
	public const FG_BLACK = 30;
	public const FG_RED = 31;
	public const FG_GREEN = 32;
	public const FG_YELLOW = 33;
	public const FG_BLUE = 34;
	public const FG_MAGENTA = 35;
	public const FG_CYAN = 36;
	public const FG_WHITE = 37;
	public const FG_BRIGHT_BLACK = 90;
	public const FG_BRIGHT_RED = 91;
	public const FG_BRIGHT_GREEN = 92;
	public const FG_BRIGHT_YELLOW = 93;
	public const FG_BRIGHT_BLUE = 94;
	public const FG_BRIGHT_MAGENTA = 95;
	public const FG_BRIGHT_CYAN = 96;
	public const FG_BRIGHT_WHITE = 97;

	// Background colors
	public const BG_BLACK = 40;
	public const BG_RED = 41;
	public const BG_GREEN = 42;
	public const BG_YELLOW = 43;
	public const BG_BLUE = 44;
	public const BG_MAGENTA = 45;
	public const BG_CYAN = 46;
	public const BG_WHITE = 47;
	public const BG_BRIGHT_BLACK = 100;
	public const BG_BRIGHT_RED = 101;
	public const BG_BRIGHT_GREEN = 102;
	public const BG_BRIGHT_YELLOW = 103;
	public const BG_BRIGHT_BLUE = 104;
	public const BG_BRIGHT_MAGENTA = 105;
	public const BG_BRIGHT_CYAN = 106;
	public const BG_BRIGHT_WHITE = 107;

	public const INVERT = 7;
}