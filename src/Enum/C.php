<?php declare(strict_types=1);

namespace Aviat\Kilo\Enum;

use Aviat\Kilo\Traits;

/**
 * Just a namespace for C language constants
 */
class C {
	use Traits\ConstList;

	// ------------------------------------------------------------------------
	// ! Misc I/O constants
	// ------------------------------------------------------------------------

	public const STDIN_FILENO = 0;
	public const STDOUT_FILENO = 1;
	public const STDERR_FILENO = 2;
	public const TCSAFLUSH = 2;

	// ------------------------------------------------------------------------
	// ! Termios flags and constants
	// ------------------------------------------------------------------------

	/* Input modes */
	public const IGNBRK = (1 << 0);    /* Ignore break condition.  */
	public const BRKINT = (1 << 1);    /* Signal interrupt on break.  */
	public const IGNPAR = (1 << 2);    /* Ignore characters with parity errors.  */
	public const PARMRK = (1 << 3);    /* Mark parity and framing errors.  */
	public const INPCK = (1 << 4);    /* Enable input parity check.  */
	public const ISTRIP = (1 << 5);    /* Strip 8th bit off characters.  */
	public const INLCR = (1 << 6);    /* Map NL to CR on input.  */
	public const IGNCR = (1 << 7);    /* Ignore CR.  */
	public const ICRNL = (1 << 8);    /* Map CR to NL on input.  */
	public const IXON = (1 << 9);    /* Enable start/stop output control.  */
	public const IXOFF = (1 << 10);    /* Enable start/stop input control.  */
	public const IXANY = (1 << 11);    /* Any character will restart after stop.  */
	public const IMAXBEL = (1 << 13);    /* Ring bell when input queue is full.  */
	public const IUCLC = (1 << 14);    /* Translate upper case input to lower case. */

	/* Output modes */
	public const OPOST = (1 << 0);    /* Perform output processing.  */
	public const ONLCR = (1 << 1);    /* Map NL to CR-NL on output.  */
	public const OXTABS = (1 << 2);        /* Expand tabs to spaces.  */
	public const ONOEOT = (1 << 3);    /* Discard EOT (^D) on output.  */
	public const OCRNL = (1 << 4);    /* Map CR to NL.  */
	public const ONOCR = (1 << 5);    /* Discard CR's when on column 0.  */
	public const ONLRET = (1 << 6);    /* Move to column 0 on NL.  */
	public const NLDLY = (3 << 8);    /* NL delay.  */
	public const NL0 = (0 << 8);    /* NL type 0.  */
	public const NL1 = (1 << 8);    /* NL type 1.  */
	public const TABDLY = (3 << 10 | 1 << 2);    /* TAB delay.  */
	public const TAB0 = (0 << 10);    /* TAB delay type 0.  */
	public const TAB1 = (1 << 10);    /* TAB delay type 1.  */
	public const TAB2 = (2 << 10);    /* TAB delay type 2.  */
	public const TAB3 = (1 << 2);    /* Expand tabs to spaces.  */
	public const CRDLY = (3 << 12);    /* CR delay.  */
	public const CR0 = (0 << 12);    /* CR delay type 0.  */
	public const CR1 = (1 << 12);    /* CR delay type 1.  */
	public const CR2 = (2 << 12);    /* CR delay type 2.  */
	public const CR3 = (3 << 12);    /* CR delay type 3.  */
	public const FFDLY = (1 << 14);    /* FF delay.  */
	public const FF0 = (0 << 14);    /* FF delay type 0.  */
	public const FF1 = (1 << 14);    /* FF delay type 1.  */
	public const BSDLY = (1 << 15);    /* BS delay.  */
	public const BS0 = (0 << 15);    /* BS delay type 0.  */
	public const BS1 = (1 << 15);    /* BS delay type 1.  */
	public const VTDLY = (1 << 16);    /* VT delay.  */
	public const VT0 = (0 << 16);    /* VT delay type 0.  */
	public const VT1 = (1 << 16);    /* VT delay type 1.  */
	public const OLCUC = (1 << 17);    /* Translate lower case output to upper case */
	public const OFILL = (1 << 18);    /* Send fill characters for delays.  */
	public const OFDEL = (1 << 19);    /* Fill is DEL.  */

	/* Control modes */
	public const CIGNORE = (1 << 0);    /* Ignore these control flags.  */
	public const CS5 = 0;        /* 5 bits per byte.  */
	public const CS6 = (1 << 8);    /* 6 bits per byte.  */
	public const CS7 = (1 << 9);    /* 7 bits per byte.  */
	public const CS8 = (C::CS6|C::CS7);    /* 8 bits per byte.  */
	public const CSIZE = (C::CS5|C::CS6|C::CS7|C::CS8);    /* Number of bits per byte (mask).  */
	public const CSTOPB = (1 << 10);    /* Two stop bits instead of one.  */
	public const CREAD = (1 << 11);    /* Enable receiver.  */
	public const PARENB = (1 << 12);    /* Parity enable.  */
	public const PARODD = (1 << 13);    /* Odd parity instead of even.  */
	public const HUPCL = (1 << 14);    /* Hang up on last close.  */
	public const CLOCAL = (1 << 15);    /* Ignore modem status lines.  */
	public const CRTSCTS = (1 << 16);    /* RTS/CTS flow control.  */
	public const CRTS_IFLOW = C::CRTSCTS;        /* Compatibility.  */
	public const CCTS_OFLOW = C::CRTSCTS;        /* Compatibility.  */
	public const CDTRCTS = (1 << 17);    /* DTR/CTS flow control.  */
	public const MDMBUF = (1 << 20);    /* DTR/DCD flow control.  */
	public const CHWFLOW = (C::MDMBUF|C::CRTSCTS|C::CDTRCTS); /* All types of flow control.  */

	/* Local modes */
	public const ECHOKE = (1 << 0);    /* Visual erase for KILL.  */
	public const _ECHOE = (1 << 1);    /* Visual erase for ERASE.  */
	public const ECHOE = C::_ECHOE;
	public const _ECHOK = (1 << 2);    /* Echo NL after KILL.  */
	public const ECHOK = C::_ECHOK;
	public const _ECHO = (1 << 3);    /* Enable echo.  */
	public const ECHO = C::_ECHO;
	public const _ECHONL = (1 << 4);    /* Echo NL even if ECHO is off.  */
	public const ECHONL = C::_ECHONL;
	public const ECHOPRT = (1 << 5);    /* Hardcopy visual erase.  */
	public const ECHOCTL = (1 << 6);    /* Echo control characters as ^X.  */
	public const _ISIG = (1 << 7);    /* Enable signals.  */
	public const ISIG = C::_ISIG;
	public const _ICANON = (1 << 8);    /* Do erase and kill processing.  */
	public const ICANON = C::_ICANON;
	public const ALTWERASE = (1 << 9);    /* Alternate WERASE algorithm.  */
	public const _IEXTEN = (1 << 10);    /* Enable DISCARD and LNEXT.  */
	public const IEXTEN = C::_IEXTEN;
	public const EXTPROC = (1 << 11);    /* External processing.  */
	public const _TOSTOP = (1 << 22);    /* Send SIGTTOU for background output.  */
	public const TOSTOP = C::_TOSTOP;
	public const FLUSHO = (1 << 23);    /* Output being flushed (state).  */
	public const XCASE = (1 << 24);    /* Canonical upper/lower case.  */
	public const NOKERNINFO = (1 << 25);    /* Disable VSTATUS.  */
	public const PENDIN = (1 << 29);    /* Retype pending input (state).  */
	public const _NOFLSH = (1 << 31);    /* Disable flush after interrupt.  */
	public const NOFLSH = C::_NOFLSH;

	/* Control characters */
	public const VEOF = 0;        /* End-of-file character [ICANON].  */
	public const VEOL = 1;        /* End-of-line character [ICANON].  */
	public const VEOL2 = 2;        /* Second EOL character [ICANON].  */
	public const VERASE = 3;        /* Erase character [ICANON].  */
	public const VWERASE = 4;        /* Word-erase character [ICANON].  */
	public const VKILL = 5;        /* Kill-line character [ICANON].  */
	public const VREPRINT = 6;        /* Reprint-line character [ICANON].  */
	public const VINTR = 8;        /* Interrupt character [ISIG].  */
	public const VQUIT = 9;        /* Quit character [ISIG].  */
	public const VSUSP = 10;        /* Suspend character [ISIG].  */
	public const VDSUSP = 11;        /* Delayed suspend character [ISIG].  */
	public const VSTART = 12;        /* Start (X-ON) character [IXON, IXOFF].  */
	public const VSTOP = 13;        /* Stop (X-OFF) character [IXON, IXOFF].  */
	public const VLNEXT = 14;        /* Literal-next character [IEXTEN].  */
	public const VDISCARD = 15;        /* Discard character [IEXTEN].  */
	public const VMIN = 16;        /* Minimum number of bytes read at once [!ICANON].  */
	public const VTIME = 17;        /* Time-out value (tenths of a second) [!ICANON].  */
	public const VSTATUS = 18;        /* Status character [ICANON].  */
	public const NCCS = 20;        /* Value duplicated in <hurd/tioctl.defs>.  */

	// ------------------------------------------------------------------------
	// ! IOCTL constants
	// ------------------------------------------------------------------------
	public const TIOCGWINSZ = 0x5413;
}