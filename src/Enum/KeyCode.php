<?php declare(strict_types=1);

namespace Aviat\Kilo\Enum;

use Aviat\Kilo\Traits;
use function Aviat\Kilo\ctrl_key;

/**
 * 'Raw' input from stdin
 */
class KeyCode {
	use Traits\ConstList;

	public const ARROW_DOWN = "\e[B";
	public const ARROW_LEFT = "\e[D";
	public const ARROW_RIGHT = "\e[C";
	public const ARROW_UP = "\e[A";
	public const BACKSPACE = "\x7f";
	public const CARRIAGE_RETURN = "\r";
	public const DEL_KEY = "\e[3~";
	public const EMPTY = '';
	public const ENTER = "\r";
	public const ESCAPE = "\e";
	public const FORM_FEED = "\f";
	public const NEWLINE = "\n";
	public const NULL = "\0";
	public const PAGE_DOWN = "\e[6~";
	public const PAGE_UP = "\e[5~";
	public const SPACE = ' ';
	public const TAB = "\t";
	public const VERTICAL_TAB = "\v";

	/**
	 * Returns the ascii character for the specified
	 * ctrl + letter combo
	 *
	 * @param string $char
	 * @return string
	 */
	public static function CTRL(string $char): ?string
	{
		$char = strtolower($char);
		$ord = ord($char);

		// a = 0x61
		// z = 0x7a
		// So, 0x60 < $ord < 0x7b
		if ($ord > 0x60 && $ord < 0x7b)
		{
			return chr(ctrl_key($char));
		}

		// Invalid input, not an ascii letter
		return NULL;
	}
}
