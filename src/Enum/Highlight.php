<?php declare(strict_types=1);

namespace Aviat\Kilo\Enum;

use Aviat\Kilo\Traits;

class Highlight {
	use Traits\ConstList;

	public const NORMAL = 0;
	public const COMMENT = 1;
	public const ML_COMMENT = 2;
	public const KEYWORD1 = 3;
	public const KEYWORD2 = 4;
	public const STRING = 5;
	public const NUMBER = 6;
	public const OPERATOR = 7;
	public const VARIABLE = 8;
	public const DELIMITER = 9;
	public const INVALID = 10;
	public const MATCH = 11;
}