/**
 * Interfaces for PHP FFI
 *
 * Most of the structure code is cribbed from GLib
 *
 * Defines are not (generally) recognized by the FFI integration
 */

// PHP 'constants' for FFI integration
// These seem to be the only define statements supported by the FFI integration
#define FFI_SCOPE "terminal"
#define FFI_LIB "libc.so.6"

// Nonsense for a test with a single quote
// Ignored by PHP due to the octothorpe (#)
#if 0
#	char* x = "String with \" escape char";
#	char y = 'q';
#endif

// -----------------------------------------------------------------------------
//! <termios.h>
// -----------------------------------------------------------------------------

/* Type of terminal control flag masks.  */
typedef unsigned long int tcflag_t;

/* Type of control characters.  */
typedef unsigned char cc_t;

/* Type of baud rate specifiers.  */
typedef long int speed_t;

/* Terminal control structure.  */
struct termios
{
  /* Input modes.  */
  tcflag_t c_iflag;

  /* Output modes.  */
  tcflag_t c_oflag;

  /* Control modes.  */
  tcflag_t c_cflag;

  /* Local modes.  */
  tcflag_t c_lflag;

  /* Control characters.  */
  cc_t c_cc[20];

  /* Input and output baud rates.  */
  speed_t __ispeed, __ospeed;
};

int tcgetattr (int fd, struct termios *termios_p);
int tcsetattr (int fd, int optional_actions, const struct termios *termios_p);

// -----------------------------------------------------------------------------
//! <sys/ioctl.h>
// -----------------------------------------------------------------------------
struct winsize {
	unsigned short ws_row;
	unsigned short ws_col;
	unsigned short ws_xpixel;
	unsigned short ws_ypixel;
};
int ioctl (int, int, ...);
