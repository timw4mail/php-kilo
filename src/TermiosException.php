<?php declare(strict_types=1);

namespace Aviat\Kilo;

use Throwable;

class TermiosException extends \UnexpectedValueException {
	public function __construct($message = 'Failed to apply terminal settings', $code = 0, Throwable $previous = NULL)
	{
		parent::__construct($message, $code, $previous);
	}
}