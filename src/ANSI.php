<?php declare(strict_types=1);

namespace Aviat\Kilo;

/**
 * ANSI
 */
class ANSI {
	// ------------------------------------------------------------------------
	// General ANSI standard escape sequences
	// ------------------------------------------------------------------------

	/**
	 * Clear the terminal window
	 */
	public const CLEAR_SCREEN = "\e[2J";

	/**
	 * Clear the terminal line
	 */
	public const CLEAR_LINE = "\e[K";

	// ------------------------------------------------------------------------
	// Text formatting escape sequences
	// ------------------------------------------------------------------------

	/**
	 * Removes text attributes, such as bold, underline, blink, inverted colors
	 */
	public const RESET_TEXT = "\e[0m";

	public const BOLD_TEXT = "\e[1m";

	public const UNDERLINE_TEXT = "\e[4m";

	/**
	 * Move the cursor to position 0,0 which is the top left
	 */
	public const RESET_CURSOR = "\e[H";

	// ------------------------------------------------------------------------
	// VT-series escapes, not really ANSI standard
	// ------------------------------------------------------------------------

	public const HIDE_CURSOR = "\e[?25l";
	public const SHOW_CURSOR = "\e[?25h";

	/**
	 * Generate the ascii sequence for basic text color
	 *
	 * @param int $color
	 * @return string
	 */
	public static function color(int $color): string
	{
		return self::seq('%dm', $color);
	}

	/**
	 * Generate a sequence for an rgb color
	 *
	 * @param int $r
	 * @param int $g
	 * @param int $b
	 * @return string
	 */
	public static function rgbColor(int $r, int $g, int $b): string
	{
		return self::seq('38;2;%d;%d;%dm', $r, $g, $b);
	}

	/**
	 * Move the cursor the specified position
	 *
	 * @param int $line
	 * @param int $column
	 * @return string
	 */
	public static function moveCursor(int $line, int $column): string
	{
		return self::seq('%d;%dH', $line, $column);
	}

	/**
	 * Scroll the specified number of lines up
	 *
	 * @param int $lines
	 * @return string
	 */
	public static function scrollUp(int $lines): string
	{
		return self::seq('%dS', $lines);
	}

	/**
	 * Scroll the specified number of lines down
	 *
	 * @param int $lines
	 * @return string
	 */
	public static function scrollDown(int $lines): string
	{
		return self::seq('%dT', $lines);
	}

	/**
	 * Simplify creating ansi escape codes
	 *
	 * @param string $pattern
	 * @param mixed ...$args
	 * @return string
	 */
	private static function seq(string $pattern, ...$args): string
	{
		return sprintf("\e[{$pattern}", ...$args);
	}
}